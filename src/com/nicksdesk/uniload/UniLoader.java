package com.nicksdesk.uniload;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.stream.Stream;

public class UniLoader {
	
	private String fPath = "";
	private Properties config = new Properties();
	private Class<?> currentClass = null;
	
	public UniLoader(String fPath) {
		this.fPath = fPath;
	}
	
	public void loadPlugin() {
		try(Stream<Path> paths = Files.walk(Paths.get(this.fPath))) {
			paths.filter(Files::isRegularFile).forEach(this::tryLoading);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void tryLoading(Path p) {
		//System.out.println(p.getFileName());
		String classPath = "";
		try {
			if(p.getFileName().endsWith(".uni")) {
				FileInputStream input = new FileInputStream(p.toFile());
				config.load(input);
				classPath = config.getProperty("exec");
			}
			if(p.getFileName().endsWith(".jar")) {
				System.out.println(p.getFileName());
				URLClassLoader child = new URLClassLoader(
					new URL[] {p.toUri().toURL()},
					this.getClass().getClassLoader()
				);
				currentClass = Class.forName(classPath, true, child);	
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public <T> Object useFunction(String methodName) {
		try {
			@SuppressWarnings("unchecked")
			Method func = ((Class<T>) ((currentClass != null) ? currentClass : new Object())).getDeclaredMethod(methodName);
			Object inst = currentClass.newInstance();
			return func.invoke(inst);
		} catch(Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
}
